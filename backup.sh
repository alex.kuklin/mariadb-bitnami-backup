#!/bin/bash

set -e

[ -d /bitnami/mariadb/backup/ ] || mkdir -p /bitnami/mariadb/backup/

timestamp=$EPOCHSECONDS
/opt/bitnami/mariadb/bin/mariabackup --backup --datadir=/bitnami/mariadb/data/ --target-dir=/bitnami/mariadb/backup/$timestamp -H $MYSQL_MARIADB_PORT_3306_TCP_ADDR -u root --password=$MARIADB_ROOT_PASSWORD  
/bin/tar -czf /bitnami/mariadb/backup/$timestamp.tgz -C /bitnami/mariadb/backup/${PREFIX}$timestamp -v .
/usr/bin/s3cmd -c /tmp/s3conf/.s3cfg put /bitnami/mariadb/backup/${PREFIX}$timestamp.tgz s3://saa-app-backup/
/bin/rm -Rf /bitnami/mariadb/backup/${PREFIX}$timestamp.tgz /bitnami/mariadb/backup/${PREFIX}timestamp
